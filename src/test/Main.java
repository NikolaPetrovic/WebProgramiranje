package test;

//import iz spark biblioteke
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.get;
import static spark.Spark.staticFiles;
import spark.Session;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import data.DataController;
import funcs.SystemFuncs;
import models.Disk;
import models.Kategorija;
import models.Korisnik;
import models.Organizacija;
import models.Uloga;
import models.VM;

public class Main {
	
	private static Gson gson = new Gson();
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		SystemFuncs.ucitajPodatke();
		port(8080);
		
		staticFiles.externalLocation(new File("./static").getCanonicalPath());
		
		get("/", (request,response) ->{
			response.redirect("login.html");
			return null;
		});
		
		post("/test/login", (request,response) -> {
			
			response.type("application/json");
			String str = request.body();
			Korisnik k = gson.fromJson(str, Korisnik.class);
			Session ses = request.session(true);
			
			Korisnik korIzSesije = ses.attribute("korisnik"); 
			
//			DataController d = new DataController();
//			ArrayList<Korisnik> lista = d.getListaKorisnika();
			
			for(Korisnik kor : DataController.listaKorisnika) {
				//System.out.println(kor);
				if(kor.getEmail().equals(k.getEmail()) && kor.getLozinka().equals(k.getLozinka())) {
					if(korIzSesije == null) {
						korIzSesije = kor;
						ses.attribute("korisnik",korIzSesije);
					}
					//System.out.println(korIzSesije.getUloga() + "aaloooooooo");
					
					return korIzSesije.getUloga();
					
				}
			}
			
			return false;
		});
		
		post("/test/izlistajOrganizacije", (request,response) -> {
			
			Korisnik korisnik = request.session().attribute("korisnik");
			
			if(korisnik == null || korisnik.getUloga() != Uloga.SUPER_ADMIN) {
				response.status(403); //vrati mi forbiden status ako nije super adm
				
				return "";
			}
			JsonArray jsonArr = new JsonArray();
			
			for(Organizacija o : DataController.listaOrganizacija) {
				JsonObject jo = new JsonObject();
				jo.addProperty("imeOrganizacije", o.getImeOrganizacije());
				jo.addProperty("opisOrganizacije", o.getOpisOrganizacije());
				jo.addProperty("logoOrganizacije", o.getLogoOrganizacije());
				
				jsonArr.add(jo);
				
			}
			System.out.println("---------------");
			System.out.println(jsonArr);
			
			return gson.toJson(jsonArr);
		});
		
		get("/test/odjaviSe", (request, response) -> {
			
			Session session = request.session();
			Korisnik k = session.attribute("korisnik");
			
			if(k != null) {
				session.invalidate();
			}
			return true;
			
			
		});
		
		post("/test/dodajOrganizaciju", (request,response) -> {
			
			String imeOrganizacije = "";
			String prvoIme = "";
			String opisOrganizacije = "";
			String logo = "";
			
			request.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("static/source/logos/"));
			Part uploadFile = request.raw().getPart("file");
			
			Map<String, String[]> parametri = request.raw().getParameterMap();
			
			imeOrganizacije = Arrays.toString(parametri.get("imeOrganizacije")).replace("[","").replace("]", "");
			opisOrganizacije = Arrays.toString(parametri.get("opisOrganizacije")).replace("[","").replace("]", "");
			prvoIme = Arrays.toString(parametri.get("prvoIme")).replace("[","").replace("]", "");
			
			if (!prvoIme.equals("null")) {
				
				for(Organizacija o : DataController.listaOrganizacija) {
					if(o.getImeOrganizacije().equalsIgnoreCase(prvoIme)) {
						o.setImeOrganizacije(imeOrganizacije);
						o.setOpisOrganizacije(opisOrganizacije);
						o.setLogoOrganizacije(SystemFuncs.preuzmiLogo(uploadFile, o.getLogoOrganizacije()));
						break;
					}
				}	
				for (Korisnik k : DataController.listaKorisnika) {
					try {
						if(k.getOrganizacija().equals(prvoIme)) {
							k.setOrganizacija(imeOrganizacije);
							break;
						}
					} catch (NullPointerException e) {
						
					}
					
				}

				for (VM vm : DataController.listaVM) {
					if(vm.getOrganizacijaVM().equals(prvoIme)) {
						vm.setOrganizacijaVM(imeOrganizacije);
						for (Disk d : vm.getListaDiskova()) {
							d.setOrganizacija(imeOrganizacije);
						}
						break;
					}
					
					
				}
				
				for (Disk d : DataController.listaDiskova) {
					if(d.getOrganizacija().equals(prvoIme)) {
						d.setOrganizacija(imeOrganizacije);
						break;
					}
					
					
				}
				
			}else {
				logo =SystemFuncs.preuzmiLogo(uploadFile, null);
				DataController.listaOrganizacija.add(new Organizacija(imeOrganizacije,opisOrganizacije,logo,new ArrayList<Korisnik>(),new ArrayList<String>()));
			}
			
			DataController.sacuvajOrganizacije();
			DataController.sacuvajKorisnike();
			DataController.sacuvamVM();
			DataController.sacuvajDiskove();
			
			return 1;
		});
		
		post("/test/proveriImeOrg", (request,response)->{
			return SystemFuncs.proveriImeOrg(request.body());
		});
		
		post("/test/dodajKorisnike", (request, response) ->{ 
			Session ss = request.session();
			Korisnik kk = ss.attribute("korisnik");
			JsonArray jar = new JsonArray();
			
			if(kk.getUloga()==Uloga.SUPER_ADMIN) {
				
				for (Korisnik k : DataController.listaKorisnika) {
					if(k.getUloga()!=Uloga.SUPER_ADMIN) {
						JsonObject job = new JsonObject();
						job.addProperty("email", k.getEmail());
						job.addProperty("ime", k.getIme());
						job.addProperty("prezime", k.getPrezime());
						job.addProperty("organizacija", k.getOrganizacija());
						
						jar.add(job);
					}
				}
			}else if(kk.getUloga() == Uloga.ADMIN) {
				for (Korisnik k : DataController.listaKorisnika) {
					if(k.getUloga()!=Uloga.SUPER_ADMIN && k.getOrganizacija().equals(kk.getOrganizacija())) {
						JsonObject job = new JsonObject();
						job.addProperty("email", k.getEmail());
						job.addProperty("ime", k.getIme());
						job.addProperty("prezime", k.getPrezime());
				
						jar.add(job);
					}
				}
				
			}
			
			
			
			return gson.toJson(jar);
		});
		
		post("/test/proveriEmail", (request,response) ->{
			return SystemFuncs.proveriEmailKor(request.body());
		});
		
		post("/test/ucitajKorisnika_u_bazu", (request,response) ->{
			response.type("application/json");
			String payload = request.body();
			
			Session ss = request.session();
			
			Korisnik logged = ss.attribute("korisnik");
			
			Korisnik k = gson.fromJson(payload, Korisnik.class);
			
			if(logged.getUloga()==Uloga.ADMIN) {
				k.setOrganizacija(logged.getOrganizacija());
			}
		
			boolean done = DataController.dodajKorisnika(k);
			return done;
		});
		
		post("/test/prikaziVM", (request,response) ->{
			
			Session ss = request.session();
			Korisnik k = ss.attribute("korisnik");
			
			if (k!=null) {
				if(k.getUloga()==Uloga.ADMIN || k.getUloga()==Uloga.KORISNIK) {
					return gson.toJson(DataController.nadjiVmNaOsnovuImenaOrganizacije(k.getOrganizacija()));
				}else {
					return gson.toJson(DataController.listaVM);
				}
			}
			
			return "";
			
		});
		
		post("/test/prikaziKategorije", (request, response) ->{
			response.type("application/json");
			
			return gson.toJson(DataController.preuzmiFormatiraneKategorije());
		});
		
		
		post("/test/izlistajDiskove", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			String org = job.get("org").getAsString();
			
			if(logged.getUloga()==Uloga.ADMIN) {
				org = logged.getOrganizacija();
			}
			
			System.out.println("^^^^^^^^^^^^^^^^^^^" + job + "&&&&");
			
			return gson.toJson(DataController.dobaviSveVMNaOsnovuOrganizacije(org));
		});
		
		
		post("/test/dodajVM", (request,response)->{
			response.type("application/json");
			String payload = request.body();
			Korisnik k = request.session().attribute("korisnik");
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
			
			if(k.getUloga()==Uloga.ADMIN) {
				job.addProperty("organizacijaVM", k.getOrganizacija());
			}
			
			DataController.dodajVM(job);
			
			return true;
			
			
			
		});
		
		post("/test/proveriVMime", (request, response) ->{
			
			return SystemFuncs.proveriVMime(request.body());
			
		});
		
		post("/test/dobaviVM", (request, response)->{
			response.type("application/json");
			
			Korisnik logged = request.session().attribute("korisnik");
			
			String payload = request.body();
			
			JsonParser jp = new JsonParser();
			
			JsonObject job = jp.parse(payload).getAsJsonObject();
			
			String vmIme = job.get("imeVM").getAsString();
			String vmOrg = job.get("org").getAsString();
			
			if(logged.getUloga()==Uloga.ADMIN) {
				vmOrg = logged.getOrganizacija();
			}
			
			job = new JsonObject();
			job.add("vm", jp.parse(gson.toJson(DataController.preuzmiVMPoImenu(vmIme))));
			
			job.add("svi", jp.parse(gson.toJson(DataController.preuzmiSlobodneDiskoveNaOsnovuOrganizacije(vmOrg))));
			
			
			job.add("sveKat", jp.parse(gson.toJson(DataController.listaKategorija)));
			
	
			return gson.toJson(job);
		});
		
		
		post("/test/diskStranica", (requst,response) -> {
			
			response.type("application/json");
			
			Korisnik logged = requst.session().attribute("korisnik");
			if(logged.getUloga() == Uloga.ADMIN || logged.getUloga() == Uloga.KORISNIK) {
				ArrayList<Disk> diskovi = new ArrayList<Disk>(DataController.listaDiskova);
				diskovi.removeIf(d -> !d.getOrganizacija().equals(logged.getOrganizacija()));
				return gson.toJson(diskovi);	
			}
			
			return gson.toJson(DataController.listaDiskova);	
			
		});
		
		post("/test/dodajDisk", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			
			if(logged.getUloga()==Uloga.ADMIN) {
				job.addProperty("org", logged.getOrganizacija());
				
			}
			
			return DataController.dodajDisk(job);
		});
		
		post("/test/proveriImeDisku", (req, res) ->{
			
			return SystemFuncs.proveriImeDisku(req.body());
		});
		
		
		post("/test/popuniKategorije", (req, res) ->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
		
			
			return gson.toJson(DataController.listaKategorija);	
		});
			
		post("/test/dodajKategoriju", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			Kategorija k = gson.fromJson(req.body(), Kategorija.class);
			
			
			
			return DataController.dodajKategoriju(k);
		});
		
		post("/test/provKatIme", (req, res) ->{
			
			return SystemFuncs.KatImePostoji(req.body());
		});
		
		get("/test/prikaziProfil", (req, res) ->{
			res.type("application/json");
			Korisnik k = req.session().attribute("korisnik");
			return gson.toJson(k);
		});
		
		post("/test/izmeniKorisnika", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			Korisnik k = gson.fromJson(payload, Korisnik.class);
			
			Korisnik old = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
			String oldMail;
			try {
				 oldMail= job.get("oldEmail").getAsString();
			} catch (NullPointerException e) {
				oldMail=null;
			}
			
			
			boolean done;
			if(oldMail !=null) {
				 Korisnik newK = DataController.updateProfile(k, old);
				 req.session().attribute("korisnik", newK);
				 return true;
			}else {
				done = DataController.updateKorisnik(k);
				return done;
			}

			
			
		});
		
		post("/test/popuniAdmin", (req, res) ->{
			Session ss = req.session();
			
			Korisnik k = ss.attribute("korisnik");
			
			if (k == null || k.getUloga()!=Uloga.ADMIN) {
				res.status(403);
				return "";
			}
			
			Organizacija org = DataController.preuzmiOrgPoImenu(k.getOrganizacija());
			
			System.out.println(org.toString());
			
			return gson.toJson(org);
		});
		post("/test/proveriPristup", (req, res) ->{
					
			Session ss = req.session();
			Korisnik kk = ss.attribute("korisnik");
					
			if(kk == null || kk.getUloga() != Uloga.KORISNIK ) {
				res.status(403);
			}
			
			
			return "";
					
		});
		
		post("/test/izmeniKorisnika", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			Korisnik k = gson.fromJson(payload, Korisnik.class);
			
			Korisnik old = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
			String oldMail;
			try {
				 oldMail= job.get("oldEmail").getAsString();
			} catch (NullPointerException e) {
				oldMail=null;
			}
			boolean done;
			
			if(oldMail !=null) {
				 Korisnik newK = DataController.updateProfile(k, old);
				 req.session().attribute("korisnik", newK);
				 return true;
			}else {
				done = DataController.updateKorisnik(k);
				return done;
			}

			
			
		});
		
		post("/test/obrisiKorisnika", (req, res) ->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
	
			String payload = req.body();
			Korisnik k = gson.fromJson(payload, Korisnik.class);
			
			if(logged.getEmail().equals(k.getEmail())) {
				res.status(400);
				return "Ne mo�ete obrisati sebe";	
			}
			
			boolean done = DataController.izbrisiKorPoMailu(k.getEmail());
			return done;
		});
		
	

	}

}
