package models;

public class Kategorija {
	
	public String ime;
	public int brojJezgara;
	public int RAM;
	public int GPUjezgra;
	public Kategorija(String ime, int brojJezgara, int rAM, int gPUjezgra) {
		super();
		this.ime = ime;
		this.brojJezgara = brojJezgara;
		RAM = rAM;
		GPUjezgra = gPUjezgra;
	}
	public Kategorija() {
		super();
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public int getBrojJezgara() {
		return brojJezgara;
	}
	public void setBrojJezgara(int brojJezgara) {
		this.brojJezgara = brojJezgara;
	}
	public int getRAM() {
		return RAM;
	}
	public void setRAM(int rAM) {
		RAM = rAM;
	}
	public int getGPUjezgra() {
		return GPUjezgra;
	}
	public void setGPUjezgra(int gPUjezgra) {
		GPUjezgra = gPUjezgra;
	}
	@Override
	public String toString() {
		return "Kategorija [ime=" + ime + ", brojJezgara=" + brojJezgara + ", RAM=" + RAM + ", GPUjezgra=" + GPUjezgra
				+ "]";
	}
	
	

}
