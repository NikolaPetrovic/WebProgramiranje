package models;

import java.util.ArrayList;

public class VM {
	
	private String imeVM;
	private String organizacijaVM;
	private Kategorija kategorija;
	private ArrayList<Disk> listaDiskova;
	private ArrayList<Aktivnosti> listaAktivnosti;
	
	
	public VM() {
		super();
	}
	
	public VM(String imeVM, String organizacijaVM, Kategorija kategorija, ArrayList<Disk> listaDiskova,
			ArrayList<Aktivnosti> listaAktivnosti) {
		super();
		this.imeVM = imeVM;
		this.organizacijaVM = organizacijaVM;
		this.kategorija = kategorija;
		this.listaDiskova = listaDiskova;
		this.listaAktivnosti = listaAktivnosti;
	}
	public String getImeVM() {
		return imeVM;
	}
	public void setImeVM(String imeVM) {
		this.imeVM = imeVM;
	}
	public String getOrganizacijaVM() {
		return organizacijaVM;
	}
	public void setOrganizacijaVM(String organizacijaVM) {
		this.organizacijaVM = organizacijaVM;
	}
	public Kategorija getKategorija() {
		return kategorija;
	}
	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}
	public ArrayList<Disk> getListaDiskova() {
		return listaDiskova;
	}
	public void setListaDiskova(ArrayList<Disk> listaDiskova) {
		this.listaDiskova = listaDiskova;
	}
	public ArrayList<Aktivnosti> getListaAktivnosti() {
		return listaAktivnosti;
	}
	public void setListaAktivnosti(ArrayList<Aktivnosti> listaAktivnosti) {
		this.listaAktivnosti = listaAktivnosti;
	}
	
	
	
}
