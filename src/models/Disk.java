package models;

public class Disk {
	
	private String imeDiska;
	private String organizacija;
	private TipDiska tipDiska;
	private int kapacitet;
	private String vm;
	
	public Disk(String imeDiska, String organizacija, String vm ,TipDiska tipDiska, int kapacitet) {
		super();
		this.imeDiska = imeDiska;
		this.organizacija = organizacija;
		this.tipDiska = tipDiska;
		this.kapacitet = kapacitet;
		this.vm = vm;
	}
	public Disk() {
		super();
	}
	public String getImeDiska() {
		return imeDiska;
	}
	public void setImeDiska(String imeDiska) {
		this.imeDiska = imeDiska;
	}
	public String getOrganizacija() {
		return organizacija;
	}
	public void setOrganizacija(String organizacija) {
		this.organizacija = organizacija;
	}
	public TipDiska getTipDiska() {
		return tipDiska;
	}
	public void setTipDiska(TipDiska tipDiska) {
		this.tipDiska = tipDiska;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	public String getVm() {
		return vm;
	}
	public void setVm(String vm) {
		this.vm = vm;
	}
	
	

}
