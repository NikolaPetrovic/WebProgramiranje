package models;

import java.util.Date;

public class Aktivnosti {
	
	private Date datumKadJeUpaljena;
	private Date datumKadJeUgasena;
	
	public Aktivnosti(Date datumKadJeUpaljena, Date datumKadJeUgasena) {
		super();
		this.datumKadJeUpaljena = datumKadJeUpaljena;
		this.datumKadJeUgasena = datumKadJeUgasena;
	}
	public Date getDatumKadJeUpaljena() {
		return datumKadJeUpaljena;
	}
	public void setDatumKadJeUpaljena(Date datumKadJeUpaljena) {
		this.datumKadJeUpaljena = datumKadJeUpaljena;
	}
	public Date getDatumKadJeUgasena() {
		return datumKadJeUgasena;
	}
	public void setDatumKadJeUgasena(Date datumKadJeUgasena) {
		this.datumKadJeUgasena = datumKadJeUgasena;
	}
	
	

}
