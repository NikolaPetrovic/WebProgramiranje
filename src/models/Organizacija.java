package models;

import java.util.ArrayList;

public class Organizacija {
	
	private String imeOrganizacije;
	private String opisOrganizacije;
	private String logoOrganizacije;
	private ArrayList<Korisnik> listaKorisnika;
	private ArrayList<String> listaResursa;
	
	
	
	
	public Organizacija(String imeOrganizacije, String opisOrganizacije, String logoOrganizacije,
			ArrayList<Korisnik> listaKorisnika) {
		super();
		this.imeOrganizacije = imeOrganizacije;
		this.opisOrganizacije = opisOrganizacije;
		this.logoOrganizacije = logoOrganizacije;
		this.listaKorisnika = listaKorisnika;
	}

	public Organizacija(String imeOrganizacije, String opisOrganizacije, String logoOrganizacije,
			ArrayList<Korisnik> listaKorisnika, ArrayList<String> listaResursa) {
		super();
		this.imeOrganizacije = imeOrganizacije;
		this.opisOrganizacije = opisOrganizacije;
		this.logoOrganizacije = logoOrganizacije;
		this.listaKorisnika = listaKorisnika;
		this.listaResursa = listaResursa;
	}
	
	public Organizacija() {
		super();
	}

	public String getImeOrganizacije() {
		return imeOrganizacije;
	}

	public void setImeOrganizacije(String imeOrganizacije) {
		this.imeOrganizacije = imeOrganizacije;
	}

	public String getOpisOrganizacije() {
		return opisOrganizacije;
	}

	public void setOpisOrganizacije(String opisOrganizacije) {
		this.opisOrganizacije = opisOrganizacije;
	}

	public String getLogoOrganizacije() {
		return logoOrganizacije;
	}

	public void setLogoOrganizacije(String logoOrganizacije) {
		this.logoOrganizacije = logoOrganizacije;
	}

	public ArrayList<Korisnik> getListaKorisnika() {
		return listaKorisnika;
	}

	public void setListaKorisnika(ArrayList<Korisnik> listaKorisnika) {
		this.listaKorisnika = listaKorisnika;
	}

	public ArrayList<String> getListaResursa() {
		return listaResursa;
	}

	public void setListaResursa(ArrayList<String> listaResursa) {
		this.listaResursa = listaResursa;
	}

	@Override
	public String toString() {
		return "Organizacija [imeOrganizacije=" + imeOrganizacije + ", opisOrganizacije=" + opisOrganizacije
				+ ", logoOrganizacije=" + logoOrganizacije + ", listaKorisnika=" + listaKorisnika + ", listaResursa="
				+ listaResursa + "]";
	}
	
	

}
