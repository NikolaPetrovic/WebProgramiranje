package data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import models.Aktivnosti;
import models.Disk;
import models.Kategorija;
import models.Korisnik;
import models.Organizacija;
import models.TipDiska;
import models.VM;

public class DataController {
	

	public static ArrayList<Korisnik> listaKorisnika = new ArrayList<Korisnik>();
	public static ArrayList<Organizacija> listaOrganizacija = new ArrayList<Organizacija>();
	private static FileWriter file;
	public static ArrayList<VM> listaVM = new ArrayList<VM>();
	public static ArrayList<Disk> listaDiskova = new ArrayList<Disk>();
	public static ArrayList<Kategorija> listaKategorija = new ArrayList<Kategorija>();
//	public ArrayList<Korisnik> getListaKorisnika() {
//		return listaKorisnika;
//	}
//
//	public void setListaKorisnika(ArrayList<Korisnik> listaKorisnika) {
//		this.listaKorisnika = listaKorisnika;
//	}
	public static void dodajVM(JsonObject job) {
		
		String ime = job.get("imeVM").getAsString();
		String org = job.get("organizacijaVM").getAsString();
		String kategorija = job.get("kategorija").getAsString();
		JsonArray _diskovi = job.get("listaDiskova").getAsJsonArray();
		
		ArrayList<Disk> disks = JsArrToObjDisk(_diskovi);
		
		for (Disk d1 : listaDiskova) {
			for (Disk d2 : disks) {
				if(d1.getImeDiska().equals(d2.getImeDiska())) {
					d1.setVm(ime);
					d2.setVm(ime);
				}
			}
		}
		ArrayList<Aktivnosti> prazna = new ArrayList<Aktivnosti>();
		
		listaVM.add(new VM(ime, org, preuzmiKategorijuPoImenu(kategorija), disks, prazna));
		
		for (Organizacija org2 : listaOrganizacija) {
			if(org2.getImeOrganizacije().equals(org)) {
				org2.getListaResursa().add(ime); 
				
				break;
			}
		}
		
		
		
		sacuvajOrganizacije();
		sacuvamVM();
		sacuvajDiskove();
		
	}
	public static void sacuvajKategorije() {
		String line = new Gson().toJson(listaKategorija);
		
		try {
			file = new FileWriter(new File("src/data/kategorije.json"));
			file.write(line);
			file.close();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public static void sacuvamVM() {
		
		String line = new Gson().toJson(listaVM);
		
		try {
			file = new FileWriter(new File("src/data/vms.json"));
			file.write(line);
			file.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void sacuvajDiskove() {
		
		String line = new Gson().toJson(listaDiskova);
		try {
			file = new FileWriter(new File("src/data/diskovi.json"));
			file.write(line);
			file.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void sacuvajKorisnike() {
		
		String line = new Gson().toJson(listaKorisnika);
		
		try {
			file=  new FileWriter(new File("src/data/korisnici.json"));
			file.write(line);
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void sacuvajOrganizacije() {
		String line = new Gson().toJson(listaOrganizacija);
		for(Organizacija o: listaOrganizacija) {
			System.out.println(o.getImeOrganizacije());
		}
		try {
			file = new FileWriter(new File("src/data/organizacije.json"));
			file.write(line);
			file.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean dodajKorisnika(Korisnik k) {
			
			listaKorisnika.add(k);
			
			for (Organizacija org : listaOrganizacija) {
				if(org.getImeOrganizacije().equals(k.getOrganizacija())) {
					org.getListaKorisnika().add(k);
					
					break;
				}
			}
			
			sacuvajKorisnike();
			sacuvajOrganizacije();
			
			return true;
			
		}
	public static ArrayList<VM> nadjiVmNaOsnovuImenaOrganizacije(String imeOrg){
		
		ArrayList<VM> vms = new ArrayList<VM>();
		
		for (VM vm : listaVM) {
			if(vm.getOrganizacijaVM().equals(imeOrg)) {
				vms.add(vm);
			}
		}
		
		return vms;
	}
	public static ArrayList<String> preuzmiFormatiraneKategorije(){
		
		ArrayList<String> listaKat = new ArrayList<String>();
		
		for (Kategorija kat : listaKategorija) {
			String formated = kat.getIme()+"  [brojJezgara = "+kat.getBrojJezgara()+","+
					   " RAM(GB) = "+kat.getRAM()+","+
					   " GPU(Jezgra) = "+kat.GPUjezgra+"]";
			listaKat.add(formated);
		}

		
		return listaKat;
	}
	public static ArrayList<String> preuzmiSlobodneDiskoveNaOsnovuOrganizacije(String org) {
		ArrayList<String> diskovi = new ArrayList<String>();
		
		for (Disk d : listaDiskova) {
			if(d.getOrganizacija().equals(org) && d.getVm().equals("")) {
				String dFormated = d.getImeDiska() + " ["+d.getKapacitet()+"GB]";
				diskovi.add(dFormated);
			}
		}
		return diskovi;
	}
	
	public static ArrayList<String> dobaviSveVMNaOsnovuOrganizacije(String org ){
		ArrayList<String> vms = new ArrayList<String>();
		
		for (VM vm : listaVM) {
			if(vm.getOrganizacijaVM().equals(org)) {
				vms.add(vm.getImeVM());
			}
			
		}
		
		return vms;
	}
	public static ArrayList<Disk> JsArrToObjDisk(JsonArray jar){
		
		ArrayList<Disk> diskovi = new ArrayList<Disk>();
		
		for (JsonElement d : jar) {
			
			String dime = d.getAsString().split("\\[")[0].trim();
			
			diskovi.add(preuzmiDiskPoImenu(dime));
		}
		
		return diskovi;
		
	}
	public static Disk preuzmiDiskPoImenu(String ime) {
		
		for(Disk d : listaDiskova) {
			if(d.getImeDiska().equalsIgnoreCase(ime)) {
				return d;
			}
		}
		return null;
	}
	
	public static Kategorija preuzmiKategorijuPoImenu(String ime) {
			
			for (Kategorija kat : listaKategorija) {
				if(kat.getIme().equals(ime)) {
					return kat;
				}
			}
			
			return null;
	}

	public static VM preuzmiVMPoImenu(String name) {
	
		for (VM vm : listaVM) {
			if(vm.getImeVM().equals(name)) {
				return vm;
			}
		}
		
		return null;
	}
	
	public static boolean dodajDisk(JsonObject jo) {
		
		String org = jo.get("org").getAsString();
		String ime = jo.get("imeDiska").getAsString();
		int kapacitet = jo.get("kapacitet").getAsInt();
		String tip = jo.get("tipDiska").getAsString();
		String vm = jo.get("vm").getAsString();
		
		boolean second = false;
		boolean third = false;
		
		Disk disk = new Disk(ime,org,vm,tip.equals("HDD") ? TipDiska.HDD : TipDiska.SSD, kapacitet);
		
		listaDiskova.add(disk);
		sacuvajDiskove();
		

		if(!vm.equals("")) {
			for (VM vm2 : listaVM) {
				if(vm2.getImeVM().equals(vm)) {
					vm2.getListaDiskova().add(disk);
					second = true;
					break;
				}
			}
		}else {
			second = true;
		}
		
		sacuvamVM();
		
		
		for (Organizacija org2 : listaOrganizacija) {
			if(org2.getImeOrganizacije().equals(org)) {
				org2.getListaResursa().add(ime); 
				third = true;
				break;  //kad ima isti disk i vm???
			}
		}
		
		
		
		sacuvajDiskove();
		
		
		return second && third;
	}
	
	private static String preuzmiOrgImeodKorEmail(String email) {
			
		for (Korisnik korisnik : listaKorisnika) {
			if(korisnik.getEmail().equals(email)) {
				return korisnik.getOrganizacija();
			}
		}
			
		return null;
	}
	
	public static boolean dodajKategoriju(Kategorija k) {

		listaKategorija.add(k);
		
		sacuvajKategorije();
		
		return true;
	}
	public static Korisnik updateProfile(Korisnik k, Korisnik old) {
		
		Korisnik newK= null;
		String oldEmail = old.getEmail();
		
		for (Korisnik korisnik : listaKorisnika) {
			if (korisnik.getEmail().equals(old.getEmail())) {
				korisnik.setIme(k.getIme());
				
				korisnik.setPrezime(k.getPrezime());
				
				if(!k.getLozinka().equals("")) {
					korisnik.setLozinka(k.getLozinka());
					System.out.println("aloooooooooooo");
					
				}
					
				korisnik.setEmail(k.getEmail());
				newK = korisnik;
				break;
			}
		}
		
		for (Organizacija org : listaOrganizacija) {
			if(org.getImeOrganizacije().equals(old.getOrganizacija())) {
				for (Korisnik korisnik : org.getListaKorisnika()) {
					if (korisnik.getEmail().equals(oldEmail)) {
						korisnik.setIme(k.getIme());
						korisnik.setPrezime(k.getPrezime());
						if(!k.getLozinka().equals("")) {
							korisnik.setLozinka(k.getLozinka());
							System.out.println("aloooooooooooo");
						}
							
						korisnik.setEmail(k.getEmail());
						break;
					}
				}
				break;
			}
		}
		
		sacuvajKorisnike();
		sacuvajOrganizacije();
		
		return newK;
		
	}
	public static boolean updateKorisnik(Korisnik k) {
		
			
			for (Korisnik korisnik : listaKorisnika) {
				if (korisnik.getEmail().equals(k.getEmail())) {
					korisnik.setIme(k.getIme());
					korisnik.setPrezime(k.getPrezime());
					if(!k.getLozinka().equals("")) {
						korisnik.setLozinka(k.getLozinka());
					}
					break;
				}
			}
			
			for (Organizacija org : listaOrganizacija) {
				if(org.getImeOrganizacije().equals(preuzmiOrgImeodKorEmail(k.getEmail()))) {
					for (Korisnik korisnik : org.getListaKorisnika()) {
						if (korisnik.getEmail().equals(k.getEmail())) {
							korisnik.setIme(k.getIme());
							korisnik.setPrezime(k.getPrezime());
							break;
						}
					}
					break;
				}
			}
			
			sacuvajKorisnike();
			sacuvajOrganizacije();
			
			return true;
			
		}
	
	public static Organizacija preuzmiOrgPoImenu(String name) {
		
		for (Organizacija org : listaOrganizacija) {
			if(org.getImeOrganizacije().equals(name)) {
				return org;
			}
		}
		
		return null;
	}
	
	private static Korisnik nadjiKorisnikaPoMailu(String email) {
		
		for (Korisnik k : listaKorisnika) {
			if(k.getEmail().equals(email)) {
				return k;
			}
		}
		
		return null;
	}
	
	public static boolean izbrisiKorPoMailu(String email) {
		
		boolean userPassed = false;
		boolean orgsPassed = false;
		
		for (Organizacija org : listaOrganizacija) {
			if(org.getImeOrganizacije().equals(nadjiKorisnikaPoMailu(email).getOrganizacija())) {
				for (Iterator<Korisnik> i = org.getListaKorisnika().iterator(); i.hasNext();) {
				    Korisnik k = i.next();
				    if (k.getEmail().equals(email)) {
				    	i.remove();
				    	orgsPassed = true;
				    	break;
				    }
				}
				break;
			}
		}

		for (Iterator<Korisnik> i = listaKorisnika.iterator(); i.hasNext();) {
		    Korisnik k = i.next();
		    if (k.getEmail().equals(email)) {
		    	i.remove();
		    	userPassed = true;
		    	break;
		    }
		}
		
		if (userPassed && orgsPassed) {
			sacuvajKorisnike();
			sacuvajOrganizacije();
			return true;
		}
		else {
			return false;
		}
	}

}
