package funcs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import data.DataController;
import models.Aktivnosti;
import models.Disk;
import models.Kategorija;
import models.Korisnik;
import models.Organizacija;
import models.TipDiska;
import models.Uloga;
import models.VM;
import spark.utils.IOUtils;

public class SystemFuncs {
	
	private static Gson gson = new Gson();
	private static final String logo_loc = "/source/logos/";
	private static  final String def_logo = "/source/img.png";
	private static FileWriter file;
	
	public static void ucitajPodatke() {
		System.out.println("UCITAVA");
		File f = new File("src/data/korisnici.json");
		File f2 = new File("src/data/organizacije.json");
		File f3 = new File("src/data/diskovi.json");
		File f4 = new File("src/data/vms.json");
		File f5 = new File("src/data/kategorije.json");
		
		if(f.length() == 0) {
			dodajPredefKorisnike();
		}
		if(f2.length() == 0) {
			dodajPredefOrganizaciju();
		}
		if(f3.length() == 0) {
			dodajPredefDiskove();
		}
		if(f4.length() == 0) {
			try {
				dodajPredefVM();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}if(f5.length() == 0) {
			dodajPredefKategorije();
		}
		
		try {
			FileReader fileReader = new FileReader(f);
			JsonParser jasonParser = new JsonParser();
			JsonElement jsonElem = jasonParser.parse(fileReader);
			JsonArray jsonArray = jsonElem.getAsJsonArray();
			
			for(JsonElement j : jsonArray) {
				Korisnik k = gson.fromJson(j, Korisnik.class);
				DataController.listaKorisnika.add(k);
			}
			System.err.println("*********************");
			System.out.println(jsonArray);
			System.out.println("*******************");
			
			fileReader = new FileReader(f2);
			jsonArray = jasonParser.parse(fileReader).getAsJsonArray();
			
			for(JsonElement j : jsonArray) {
				Organizacija o = gson.fromJson(j, Organizacija.class);
				DataController.listaOrganizacija.add(o);
			}
			System.err.println("*********************");
			System.out.println(jsonArray);
			System.out.println("*******************");
			
			fileReader = new FileReader(f4);
			jsonArray = jasonParser.parse(fileReader).getAsJsonArray();
			
			for (JsonElement jsonElement : jsonArray) {
				VM vm = gson.fromJson(jsonElement, VM.class);
				DataController.listaVM.add(vm);
			}
			System.out.println("*******************");
			System.out.println(jsonArray);
			System.out.println("*******************");
			
			fileReader = new FileReader(f3);
			jsonArray = jasonParser.parse(fileReader).getAsJsonArray();
			
			for (JsonElement jsonElement : jsonArray) {
				Disk d = gson.fromJson(jsonElement, Disk.class);
				DataController.listaDiskova.add(d);		
			}
			System.out.println("*******************");
			System.out.println(jsonArray);
			System.out.println("*******************");
			
			fileReader = new FileReader(f5);
			jsonArray = jasonParser.parse(fileReader).getAsJsonArray();
			
			for (JsonElement jsonElement : jsonArray) {
				Kategorija k = gson.fromJson(jsonElement, Kategorija.class);
				DataController.listaKategorija.add(k);		
			}
			System.out.println("*******************");
			System.out.println(jsonArray);
			System.out.println("*******************");
			
			fileReader.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	public static void dodajPredefKategorije() {
		
		ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>();

		kategorije.add(new Kategorija("Slow", 1, 2, 2));
		kategorije.add(new Kategorija("Medium", 4, 2, 4));
		kategorije.add(new Kategorija("Fast", 12, 16, 8));
		
		
		
		String json = new Gson().toJson(kategorije);
		
		try {
			file = new FileWriter(new File("src/data/kategorije.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}			
	}
	public static void dodajPredefKorisnike() {
		
		ArrayList<Korisnik> listaKorisnika = new ArrayList<Korisnik>();
		listaKorisnika.add(new Korisnik("sadmin@gmail.com","test","Nikola","Petrovic",null,Uloga.SUPER_ADMIN));
		listaKorisnika.add(new Korisnik("admin@gmail.com", "admin", "Peka", "Pekat", "Org1", Uloga.ADMIN));
		listaKorisnika.add(new Korisnik("user@gmail.com", "user", "Mika", "Mika", "Org2", Uloga.KORISNIK));
		
		String json = new Gson().toJson(listaKorisnika);
		
		try {
			file = new FileWriter(new File("src/data/korisnici.json"));
			file.write(json);
			file.close();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		System.out.println(listaKorisnika);
	}
	
	public static void dodajPredefOrganizaciju() {
		
		ArrayList<Organizacija> listaOrganizacija = new ArrayList<Organizacija>();
		ArrayList<Korisnik> listaKorisnika = new ArrayList<Korisnik> ();
		ArrayList<String> listaResursa = new ArrayList<String>();
		
		listaResursa.add("WindDefend");
		listaResursa.add("WSearch");
		listaResursa.add("StateRepository");
		listaResursa.add("DnsCache");
		listaResursa.add("SystemEventBroker");
		
		listaKorisnika.add(new Korisnik("admin@gmail.com", "admin", "Peka", "Pekat", "Org1", Uloga.ADMIN));
		listaKorisnika.add(new Korisnik("user@gmail.com", "user", "Mika", "Mika", "Org2", Uloga.KORISNIK));
		listaOrganizacija.add(new Organizacija("Org1","Opis1",def_logo,new ArrayList<Korisnik>(listaKorisnika),new ArrayList<String>(listaResursa)));
		
		listaResursa.clear();
		listaKorisnika.clear();
		
		listaResursa.add("Tomahawkb450Max");
		listaResursa.add("kingstonA3000");
		listaResursa.add("rx590Overkill");
		
		listaKorisnika.add(new Korisnik("user2@gmail.com", "user2", "Janko", "Jankovic", "Org2", Uloga.KORISNIK));
		listaOrganizacija.add(new Organizacija("Org2","test123", def_logo,new ArrayList<Korisnik>(listaKorisnika),new ArrayList<String>(listaResursa)));
		
		for(Organizacija o: listaOrganizacija) {
			System.out.println(o.getImeOrganizacije());
		}
		listaKorisnika.clear();
		listaResursa.clear();
		
		listaOrganizacija.add(new Organizacija("Org3", "testing3RD", def_logo, listaKorisnika, listaResursa));
		
		
		String strJson = new Gson().toJson(listaOrganizacija);
		
		try {
			file = new FileWriter(new File("src/data/organizacije.json"));
			file.write(strJson);
			file.close();
		}catch (Exception e) {
			
		}
		
	}
	public static void dodajPredefDiskove() {
	
		ArrayList<Disk> listaDiskova = new ArrayList<Disk>();
		
		listaDiskova.add(new Disk("Toshiba", "Org1", "Windows1", TipDiska.SSD, 120));
		listaDiskova.add(new Disk("KingstonA2000", "Org1", "LinuxUb", TipDiska.HDD, 1024));
		listaDiskova.add(new Disk("Seagate", "Org1", "MacOS", TipDiska.SSD, 512));
		
		listaDiskova.add(new Disk("SkyHawk","Org2","MacOs",TipDiska.SSD,120));
		listaDiskova.add(new Disk("WD Blue", "Org2", "Fedora", TipDiska.HDD, 240));
		
		listaDiskova.add(new Disk("BarraCuda", "Org1", "", TipDiska.HDD, 2048));		//slobodni
		listaDiskova.add(new Disk("KingstonA2000", "Org1", "", TipDiska.SSD, 512));
		
		listaDiskova.add(new Disk("Toshiba", "Org2", "", TipDiska.SSD, 512));
		
		String str = new Gson().toJson(listaDiskova);
		
		try {
			file = new FileWriter(new File("src/data/diskovi.json"));
			file.write(str);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}		
		
		
	}
	
	public static void dodajPredefVM() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		ArrayList<VM> listaVm = new ArrayList<VM>();
		ArrayList<Disk> listaDiskova = new ArrayList<Disk>();
		ArrayList<Aktivnosti> listaAktivnosti = new ArrayList<Aktivnosti>();
		
		Date start = sdf.parse("24/04/2004 18:30");
		Date end = sdf.parse("17/3/2006 15:15");
		listaAktivnosti.add(new Aktivnosti(start, end));
		
		start = sdf.parse("11/8/2006 07:00");
		end = sdf.parse("1/9/2006 14:58");
		listaAktivnosti.add(new Aktivnosti(start, end));
		
		start = sdf.parse("1/9/2009 21:16");
		end = sdf.parse("19/12/2009 05:33	");
		listaAktivnosti.add(new Aktivnosti(start, end));
		
		
		start = sdf.parse("12/7/2010 07:23");
		end = sdf.parse("16/9/2010 14:16");
		listaAktivnosti.add(new Aktivnosti(start, end));
		
		start = sdf.parse("01/6/2020 08:44");
		listaAktivnosti.add(new Aktivnosti(start, null));
		
		listaDiskova.add(new Disk("Toshiba", "Org1", "Windows1", TipDiska.SSD, 120));
		listaDiskova.add(new Disk("KingstonA2000", "Org1", "LinuxUb", TipDiska.HDD, 1024));
		listaDiskova.add(new Disk("Seagate", "Org1", "MacOS", TipDiska.SSD, 512));
		
		Kategorija k = new Kategorija("Slow", 1, 2, 2);
		listaVm.add(new VM("Windows1", "Org1", k, listaDiskova, listaAktivnosti));
		
		listaAktivnosti = new ArrayList<Aktivnosti>();
		
		start = sdf.parse("05/2/2020 09:16");
		listaAktivnosti.add(new Aktivnosti(start, null));
		
		listaDiskova = new ArrayList<Disk>();
		listaDiskova.add(new Disk("SkyHawk","Org2","MacOs",TipDiska.SSD,120));
		listaDiskova.add(new Disk("WD Blue", "Org2", "Fedora", TipDiska.HDD, 240));
		
		k = new Kategorija("Medium", 4, 2, 4);
		listaVm.add(new VM("LinuxUb", "Org1", k, listaDiskova, listaAktivnosti));
		listaAktivnosti = new ArrayList<Aktivnosti>();
		start = sdf.parse("12/7/2019 02:30");
		end = sdf.parse("16/9/2019 20:20");
		listaAktivnosti.add(new Aktivnosti(start, end));
		
		k = new Kategorija("Fast", 12, 16, 8);
		listaVm.add(new VM("Fedora", "Org2", k, new ArrayList<Disk>(), new ArrayList<Aktivnosti>()));
		
		
		String json = new Gson().toJson(listaVm);
		
		try {
			file = new FileWriter(new File("src/data/vms.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}

	public static String preuzmiLogo(Part uploadFile, String prviLogo) {
		
		String logo = "";
		System.out.println("KURAC2");
		
		if (uploadFile.getSubmittedFileName()!=null) {
			System.out.println("KURAC");
			
			try (InputStream inputStream = uploadFile.getInputStream()) {
                OutputStream outputStream = new FileOutputStream("static/source/logos/" + uploadFile.getSubmittedFileName());
                IOUtils.copy(inputStream, outputStream);
                outputStream.close();
            } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			logo = logo_loc + uploadFile.getSubmittedFileName();
			
		}else {
			if(prviLogo == null) {
				logo = def_logo;
				System.out.println("def logo 12134141");
			}else {
				logo = prviLogo;
				System.out.println("logo 12134141");
			}
			
		}
		
		return logo;
	}

	public static boolean proveriImeOrg(String line) {

		JsonParser jp = new JsonParser();
		String ime = jp.parse(line).getAsJsonObject().get("ime").getAsString();
		
		boolean checkName = false;
		for(Organizacija o : DataController.listaOrganizacija) {
			if(ime.equalsIgnoreCase(o.getImeOrganizacije())) {
				checkName = true;
				break;
			}
		}
		return checkName;
	}
	
	public static Object proveriEmailKor(String payload) {
		JsonParser jp = new JsonParser();
		
		String email = jp.parse(payload).getAsJsonObject().get("email").getAsString();

		boolean checkName = false;
		for (Korisnik k : DataController.listaKorisnika) {
			if(email.equals(k.getEmail())) {
				checkName = true;
				break;
			}
		}
		return checkName;
	}
	public static Object proveriVMime(String str) {
		
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(str).getAsJsonObject().get("imeVM").getAsString();

		boolean flag = false;
		for (VM vm : DataController.listaVM) {
			if(ime.equals(vm.getImeVM())) {
				flag = true;
				break;
			}
		}
		return flag;
		
	}
	
	public static Object proveriImeDisku(String payload) {
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(payload).getAsJsonObject().get("ime").getAsString();

		boolean flag = false;
		for (Disk d : DataController.listaDiskova) {
			if(ime.equals(d.getImeDiska())) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	public static Object KatImePostoji(String payload) {
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(payload).getAsJsonObject().get("ime").getAsString();

		boolean flag = false;
		for (Kategorija k : DataController.listaKategorija) {
			if(ime.equals(k.getIme())) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
}
